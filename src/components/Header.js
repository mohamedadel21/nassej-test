import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  I18nManager,
  View,
} from "react-native";
const { height, width } = Dimensions.get("window");
import { ScaleHeight } from "../utils/scale-height";
import { ScaleWidth } from "../utils/scale-width";
import Colors from "../utils/Colors";
import I18n from "../utils/i18n";
import AntDesign from "react-native-vector-icons/AntDesign";

const Header = ({
    changeLanguage,
    title
}) => {
  const styles = StyleSheet.create({
    container: {
      width,
      height: ScaleHeight(75),
      backgroundColor: Colors.blue,
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "center",
      shadowColor: Colors.black,
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.2,
      elevation:2,
      paddingTop:ScaleHeight(20)
    },
    back: {
      width: ScaleWidth(36),
      position: "absolute",
      left: ScaleWidth(10),
      bottom:ScaleHeight(20)

    },
    language: {
      position: "absolute",
      right: ScaleWidth(15),
      bottom:ScaleHeight(20)

    },
  });

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.back}>
        <AntDesign
          color={Colors.white}
          size={ScaleWidth(18)}
          name={I18nManager.isRTL ? "right" : "left"}
        />
      </TouchableOpacity>

      <TouchableOpacity style={styles.language} onPress={changeLanguage}>
        <Text style={{ alignSelf: "center", color: Colors.white }}>
          {I18n.t("Language")}
        </Text>
      </TouchableOpacity>

      <Text style={{ alignSelf: "center", color: Colors.white ,fontSize:ScaleWidth(16)}}>
        {title}
      </Text>
    </View>
  );
};

export default Header;
