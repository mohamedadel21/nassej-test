import React from "react";
import {
  Text,
  View,
  I18nManager,
  TouchableOpacity,
  StyleSheet,
  Dimensions
} from "react-native";
import { EvilIcons } from "@expo/vector-icons";
import { Input } from "react-native-elements";
import Colors from "../utils/Colors";
import { ScaleWidth } from "../utils/scale-width";
import { ScaleHeight } from "../utils/scale-height";
const { width, height } = Dimensions.get("window");

const InputComponent = (props) => {
  return (
    <TouchableOpacity
      disabled={props.disabled}
      style={[styles.container, { marginTop: props.marginTop }]}
      onPress={props.onPress}
    >
      <Input
        placeholder={props.placeholderValue}
        placeholderTextColor={Colors.darkGrey}
        value={props.value}
        autoCorrect={false}
        autoCapitalize="none"
        disabled={!props.disabled}
        textContentType={props.textContentType}
        keyboardType={props.keyboardType}
        secureTextEntry={props.secureTextEntry}
        inputContainerStyle={[
          styles.inputContainerStyle,
        ]}
        inputStyle={styles.inputStyle}
        onChangeText={props.onChangeText}
        rightIcon={
          <View>
            {props.iconName && (
                <EvilIcons
                  name={props.iconName}
                  size={props.iconSize}
                  color={props.iconColor}
                  style={{ marginRight: ScaleWidth(10) }}
                />
            )}
          </View>
        }
      ></Input>

      <Text style={styles.title}>{props.title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-start",
  },
  inputContainerStyle: {
    borderColor: Colors.grey,
    borderWidth: ScaleWidth(1),
    alignSelf: "center",
    borderRadius: ScaleWidth(5),
    width:width - ScaleWidth(40),
    height:ScaleHeight(50),
  },
  inputStyle: {
    marginLeft: ScaleWidth(20),
    fontSize: ScaleWidth(13),
    textAlign: I18nManager.isRTL ? "right" : "left",
    color:Colors.black
  },
  title: {
    position: "absolute",
    top: ScaleHeight(-7),
    left: ScaleHeight(5),
    backgroundColor: Colors.white,
    paddingHorizontal: ScaleWidth(5),
    color:Colors.dark
  },
});
export default InputComponent;
