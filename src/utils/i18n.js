"use-strict";
import I18n from "i18n-js";
import * as Updates from "expo-updates";

import { I18nManager } from "react-native";

I18n.fallbacks = true;
I18n.translations = {
  ar: require("./translations/ar"),
  en: require("./translations/en"),
};
I18n.locale = I18nManager.isRTL ? "ar" : "en";

export const handleLanguageChange = () => {
  if (I18nManager.isRTL) {
    I18nManager.forceRTL(false);
  } else {
    I18nManager.forceRTL(true);
  }

  Updates.reloadAsync();
};

export default I18n;
