export default {

    white:'#ffffff',
    black:'#000000',
    lightBlack:'#353944',
    grey:'#30303033',
    lightGrey:'#F9FBFC',
    darkGrey:'#989898',
    dark:"#757575",
    none:'#ffffff00',
    opacity:'#ffffff33',
    blue:"#007AFF",
    light:"#F4F4F4"


}


