import React, { useReducer } from "react";
import { View, Dimensions, TouchableOpacity, Text } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import styles from "./style";
import Input from "../../components/Input";
import I18n, { handleLanguageChange } from "../../utils/i18n";
import Header from "../../components/Header";
import Colors from "../../utils/Colors";
import { ScaleWidth } from "../../utils/scale-width";
import { ScaleHeight } from "../../utils/scale-height";
const { width, height } = Dimensions.get("window");
import { CheckBox, Switch } from "react-native-elements";
import DateTimePicker from "react-native-modal-datetime-picker";
import { Dialog } from "react-native-simple-dialogs";

function reducer(state, { key, value }) {
  return { ...state, [key]: value };
}

const SearchScreen = ({}) => {
  const statusList = ["status 1", "status 2", "status 3", "status 4"];
  const initailFormState = {
    name: null,
    status: I18n.t("Select Contract Status"),
    insurance: false,
    amount: null,
    armed: true,
    hijriDate: false,
    startDate: I18n.t("Contract starting date"),
    endDate: I18n.t("Contract end date"),
    showStartDate: false,
    showEndDate: false,
    showStatus: false,
  };
  const [form, updateForm] = useReducer(reducer, initailFormState);

  const selectStatus = (item) => {
    updateForm({ key: "status", value: item });
    updateForm({ key: "showStatus", value: false });
  };
  const selectStartDate = (value) => {
    let dd = String(value.getDate()).padStart(2, "0");
    let mm = String(value.getMonth() + 1).padStart(2, "0");
    let yyyy = value.getFullYear();

    updateForm({
      key: "startDate",
      value: yyyy + "-" + mm + "-" + dd,
    });
    updateForm({
      key: "showStartDate",
      value: false,
    });
  };
  const selectEndDate = (value) => {
    let dd = String(value.getDate()).padStart(2, "0");
    let mm = String(value.getMonth() + 1).padStart(2, "0");
    let yyyy = value.getFullYear();

    updateForm({
      key: "endDate",
      value: yyyy + "-" + mm + "-" + dd,
    });
    updateForm({
      key: "showEndDate",
      value: false,
    });
  };
  const onCancelStartDate = () => {
    updateForm({
      key: "showStartDate",
      value: false,
    });
  };
  const onCancelEndDate = () => {
    updateForm({
      key: "showEndDate",
      value: false,
    });
  };

  return (
    <View style={styles.container}>
      <Header
        title={I18n.t("Private Guards")}
        changeLanguage={() => handleLanguageChange()}
      />
      <KeyboardAwareScrollView>
        <Text
          style={{
            alignSelf: "flex-start",
            marginLeft: ScaleWidth(10),
            fontSize: ScaleWidth(15),
            color: Colors.blue,
            marginTop: ScaleHeight(25),
          }}
        >
          {I18n.t("Search in private guards")}
        </Text>
        <View style={styles.content}>
          <Input
            autoCorrect={false}
            title={I18n.t("Name")}
            placeholderValue={I18n.t("Name")}
            disabled={true}
            onChangeText={(value) => updateForm({ key: "name", value })}
          />
          <Input
            autoCorrect={false}
            title={I18n.t("Contract Status")}
            value={form.status}
            disabled={false}
            iconName="chevron-down"
            iconSize={ScaleWidth(30)}
            iconColor={Colors.black}
            onPress={() => updateForm({ key: "showStatus", value: true })}
          />
          <Dialog
            visible={form.showStatus}
            title={I18n.t("Select Contract Status")}
            dialogStyle={{ backgroundColor: Colors.white }}
            onTouchOutside={() =>
              updateForm({ key: "showStatus", value: false })
            }
            contentStyle={{ maxHeight: height * 0.8 }}
          >
            <View>
              {statusList.map((item, index) => {
                return (
                  <TouchableOpacity
                    key={index.toString()}
                    onPress={() => selectStatus(item)}
                  >
                    <Text style={{ fontSize: ScaleWidth(15) }}>{item}</Text>
                  </TouchableOpacity>
                );
              })}
            </View>
          </Dialog>

          <Input
            autoCorrect={false}
            title={I18n.t("Number of guards")}
            placeholderValue={I18n.t("Number of guards")}
            disabled={true}
            keyboardType="numeric"
            onChangeText={(value) => updateForm({ key: "amount", value })}
          />
          <View style={styles.row}>
            <CheckBox
              checked={form.armed}
              color={Colors.blue}
              title={I18n.t("Armed")}
              textStyle={styles.textStyle}
              containerStyle={styles.containerStyle}
              onPress={() => {
                updateForm({ key: "armed", value: true });
              }}
            />

            <CheckBox
              checked={!form.armed}
              color={Colors.blue}
              title={I18n.t("Unarmed")}
              textStyle={styles.textStyle}
              containerStyle={styles.containerStyle}
              onPress={() => {
                updateForm({ key: "armed", value: false });
              }}
            />
          </View>

          <Input
            autoCorrect={false}
            title={I18n.t("Contract starting date")}
            value={form.startDate}
            disabled={false}
            iconName="calendar"
            iconSize={ScaleWidth(30)}
            iconColor={Colors.blue}
            onPress={() =>
              updateForm({
                key: "showStartDate",
                value: true,
              })
            }
          />

          <DateTimePicker
            mode="date"
            isVisible={form.showStartDate}
            onConfirm={selectStartDate}
            onCancel={onCancelStartDate}
          />

          <View style={styles.switchContent}>
            <Text
              style={{ color: form.hijriDate ? Colors.dark : Colors.black }}
            >
              {I18n.t("Gregorian")}
            </Text>
            <Switch
              value={form.hijriDate}
              onValueChange={(value) => {
                updateForm({ key: "hijriDate", value: value });
              }}
            />
            <Text
              style={{ color: form.hijriDate ? Colors.black : Colors.dark }}
            >
              {I18n.t("Hijri")}
            </Text>
          </View>
          <Input
            autoCorrect={false}
            title={I18n.t("Contract end date")}
            value={form.endDate}
            disabled={false}
            iconName="calendar"
            iconSize={ScaleWidth(30)}
            iconColor={Colors.blue}
            onPress={() =>
              updateForm({
                key: "showEndDate",
                value: true,
              })
            }
          />

          <DateTimePicker
            mode="date"
            isVisible={form.showEndDate}
            onConfirm={selectEndDate}
            onCancel={onCancelEndDate}
          />
        </View>

        <TouchableOpacity style={styles.searchButton}>
          <Text style={styles.searchText}>{I18n.t("Search")}</Text>
        </TouchableOpacity>
      </KeyboardAwareScrollView>
    </View>
  );
};

export default SearchScreen;
