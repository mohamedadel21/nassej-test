import { StyleSheet, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");
import Colors from "../../utils/Colors";
import { ScaleWidth } from "../../utils/scale-width";
import { ScaleHeight } from "../../utils/scale-height";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  content: {
    width: width - ScaleWidth(20),
    borderRadius: ScaleWidth(10),
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Colors.white,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.2,
    elevation:2,
    padding: ScaleWidth(20),
    marginTop: ScaleHeight(20),
  },
  checkBoxText: {
    fontSize: ScaleWidth(15),
    color: Colors.black,
    marginLeft: ScaleWidth(15),
  },
  row: {
    width: width - ScaleWidth(20),
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginTop: ScaleHeight(-20),
    marginBottom: ScaleHeight(10),
  },
  switchContent: {
    width: width - ScaleWidth(20),
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginTop: ScaleHeight(-10),
    marginBottom: ScaleHeight(20),
    marginLeft: ScaleWidth(20),
    paddingHorizontal: ScaleWidth(5),
  },
  rowCheckBox: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  searchButton: {
    width: width - ScaleWidth(20),
    height: ScaleHeight(50),
    backgroundColor: Colors.blue,
    borderRadius: ScaleWidth(5),
    marginTop: ScaleHeight(40),
    justifyContent: "center",
    alignItems: "center",
  },
  searchText: {
    color: Colors.white,
    fontSize: ScaleWidth(16),
  },
  textStyle: { fontSize: 13, fontWeight: "normal" },
  containerStyle: { backgroundColor: Colors.white, borderWidth: 0 },
});
export default styles;
